#!/bin/bash
#

date="`date +'%Y-%M-%D %H:%M:%S'` "
log="/tmp/$exec_name.log"
log_level="info"
log_to_file=false

show_help() {
	cat << EOF
Help information
EOF
}

main() {
  celery flower --broker=amqp://localhost:5672/ > /dev/null 2>&1 &
  google-chrome-stable http://localhost:5555 > /dev/null 2>&1 &
}

logger() {
	level=$1
	message=$2
	suffix="\e[00m"

	case $level in
		debug)
			prefix="\e[00;32m" # Green
			case $log_level in
				debug) : ;;
				*) return ;;
			esac
			;;
		info)
			prefix="\e[00;34m" # Blue
			case $log_level in
				info|debug) : ;;
				*) return ;;
			esac
			;;
		warning)
			prefix="\e[00;33m" # Yellow
			case $log_level in
				info|debug|warning) : ;;
				*) return ;;
			esac
			;;
		error)
			prefix="\e[00;31m" # Red
			case $log_level in
				info|debug|warning|error) : ;;
				*) return ;;
			esac
			;;
		fatal)
			prefix="\e[00;31m" # Red	
			message="$message. Exiting."
			case $log_level in
				info|debug|warning|error|fatal) : ;;
				*) return ;;
			esac
			;;
	esac

	[[ $log_to_file ]] || echo "$date$message" >> $log
	echo -e $prefix$message$suffix
	
	if [[ $level == "fatal" ]]; then
		exit 1
	fi
}

while [[ $# > 0 ]]; do
	arg="$1"
	case $arg in
		-h|--help)
			show_help
			exit
			;;
		-l|--log-level)
			log_level=$2
			shift
			;;
		*)
			show_help
			exit
			;;
	esac
	shift
done

if [ $0 != "-bash" ]; then
	main
fi

